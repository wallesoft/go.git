package oauth2

import (
	"github.com/gogf/gf/v2/encoding/gjson"
	"github.com/gogf/gf/v2/errors/gcode"
)

// User
type WechatUser struct {
	Raw      *gjson.Json
	ID       string
	Name     string
	NickName string
	Avatar   string
	Email    string
	// Raw      string
}

// GetID
func (u *WechatUser) GetID() string {
	return u.ID
}

// GetName
func (u *WechatUser) GetName() string {
	return u.Name
}

// GetNickName
func (u *WechatUser) GetNickName() string {
	return u.NickName
}

// GetAvatar
func (u *WechatUser) GetAvatar() string {
	return u.Avatar
}

// GetEmail
func (u *WechatUser) GetEmail() string {
	return u.Email
}

// GetRaw return *gjson.Json
// 具体用法查看 github.com/gogf/gf/encoding/gjson
func (u *WechatUser) GetRaw() *gjson.Json {
	return u.Raw
}

// SetAccessToken
func (u *WechatUser) SetAccessToken(token string) error {
	return u.Raw.Set("access_token", token)
}

// GetAccessToken
func (u *WechatUser) GetAccessToken() string {
	return u.Raw.Get("access_token").String()
}

// SetRefreshTokne
func (u *WechatUser) SetRefreshToken(token string) error {
	return u.Raw.Set("refresh_token", token)
}

// GetRefreshToken
func (u *WechatUser) GetRefreshToken() string {
	return u.Raw.Get("refresh_token").String()
}

// SetExpiresIn
func (u *WechatUser) SetExpiresIn(expiresIn int) error {
	return u.Raw.Set("expires_in", expiresIn)
}

// GetExpiresIn
func (u *WechatUser) GetExpiresIn() int {
	return u.Raw.Get("expires_in").Int()
}

// 是否包含错误信息
func (u *WechatUser) ContainsError() bool {
	return u.Raw.Contains("errcode")
}

func (u *WechatUser) GetErrorCode() gcode.Code {
	return gcode.New(u.Raw.Get("errcode").Int(), u.Raw.Get("errmsg").String(), nil)
}
