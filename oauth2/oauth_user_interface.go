package oauth2

import (
	"github.com/gogf/gf/v2/encoding/gjson"
	"github.com/gogf/gf/v2/errors/gcode"
)

type User interface {
	// 获取ID
	GetID() string
	// 获取Name
	GetName() string
	// 获取昵称
	GetNickName() string
	// 获取头像
	GetAvatar() string
	// 获取Email
	GetEmail() string
	// 获取原始Raw gjson格式
	GetRaw() *gjson.Json
	// 是否包含错误
	ContainsError() bool
	// 获取错误code
	GetErrorCode() gcode.Code
	SetAccessToken(token string) error
	GetAccessToken() string
	SetRefreshToken(token string) error
	GetRefreshToken() string
	SetExpiresIn(int) error
	GetExpiresIn() int
}
