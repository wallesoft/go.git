package oauth2

type Config struct {
	ClientID     string   // client_id
	ClientSecret string   // client_secret
	EndPoint     EndPoint // endpoint
	Scopes       []string //scopes
	RerdirectUrl string
}

type EndPoint struct {
	AuthURL  string
	TokenURL string
}

// func (c *Config) GetClientID() string {
// 	return c.ClientID
// }

// func ()
