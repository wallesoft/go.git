package oauth2

import (
	"context"
	"net/url"
	"strings"

	"github.com/gogf/gf/v2/container/gset"
	"github.com/gogf/gf/v2/crypto/gmd5"
	"github.com/gogf/gf/v2/encoding/gjson"
	"github.com/gogf/gf/v2/errors/gerror"
	"github.com/gogf/gf/v2/net/gclient"
	"github.com/gogf/gf/v2/util/guid"
)

type WechatConfig struct {
	config         Config
	RerdirectUrl   string
	State          string
	Scopes         []string
	ComponentID    string
	ComponentToken string
	isThird        bool
	Lang           string
	// extConfig: map[string]string
}

// Wechat
func Wechat(config Config, other ...map[string]string) *WechatConfig {
	return &WechatConfig{
		config: config,
		Scopes: []string{"snsapi_login"},
	}
}

// OpenPlatform 第三方平台的配置
func (w *WechatConfig) OpenPlatform(componentID string, ComponentToken string) *WechatConfig {
	w.isThird = true
	w.ComponentID = componentID
	w.ComponentToken = ComponentToken
	return w
}

// Scope
func (w *WechatConfig) Scope(scopes []string) *WechatConfig {
	w.Scopes = scopes
	return w
}

// GetAuthURL
func (w *WechatConfig) GetAuthURL(redirectUrl string, state ...string) string {
	path := w.getPath()
	w.RerdirectUrl = redirectUrl
	// w.Scope = scope
	if len(state) > 0 {
		w.State = state[0]
	} else {
		w.State = gmd5.MustEncrypt(guid.S())
	}
	return w.buildAuthUrlFromBase("https://open.weixin.qq.com/connect/" + path)
}

// GetUserFromCode
func (w *WechatConfig) GetUserFromCode(ctx context.Context, code string) (user User, err error) {
	scope := gset.NewStrSetFrom(w.Scopes)
	if scope.Contains("snsapi_base") {
		//

		return w.toUser(w.GetTokenFromCode(ctx, code)), nil
	}
	token := w.GetTokenFromCode(ctx, code)
	user, err = w.GetUserByToken(ctx, token.Get("access_token").String())
	if err != nil {
		err = gerror.Wrap(err, "Fail get access_token by ")
		return
	}
	err = user.SetAccessToken(token.Get("access_token").String())
	if err != nil {
		err = gerror.Wrap(err, "Fail set access token.")
		return
	}
	user.SetRefreshToken(token.Get("refresh_token").String())
	user.SetExpiresIn(token.Get("expires_in").Int())
	return
}

// WithLang
func (w *WechatConfig) WithLang(lang string) *WechatConfig {
	w.Lang = lang
	return w
}

// toUser
func (w *WechatConfig) toUser(user *gjson.Json) User {
	return &WechatUser{
		Raw:      user,
		ID:       user.Get("openid").String(),
		Name:     user.Get("nickname").String(),
		NickName: user.Get("nickname").String(),
		Avatar:   user.Get("headimgurl").String(),
	}

}

// GetUserFromToken
func (w *WechatConfig) GetUserByToken(ctx context.Context, token string) (user User, err error) {
	if w.Lang == "" {
		w.Lang = "zh_CN"
	}
	client := gclient.New()
	response, _ := client.Get(ctx, w.getBaseUrl()+"/userinfo", map[string]string{
		"access_token": token,
		"openid":       w.config.ClientID,
		"lang":         w.Lang,
	})
	userRaw := gjson.New(response.ReadAll())
	user = w.toUser(userRaw)
	err = user.SetAccessToken(token)
	if err != nil {
		err = gerror.Wrap(err, "Fail to set access token")
	}
	return
}

// GetTokenFromCode
func (w *WechatConfig) GetTokenFromCode(ctx context.Context, code string) (token *gjson.Json) {
	client := gclient.New()
	response, _ := client.Header(map[string]string{
		"Accept": "application/json",
	}).Get(ctx, w.getTokenUrl(), w.getTokenFields(code).Encode())

	return gjson.New(response.ReadAll())
}

// RefreshToken
func (w *WechatConfig) buildAuthUrlFromBase(uri string) string {
	queryStr := w.getCodeFields().Encode()
	return uri + "?" + queryStr + "#wechat_redirect"
}

func (w *WechatConfig) getCodeFields() url.Values {

	val := url.Values{}
	if w.isThird && w.ComponentID != "" {
		val.Add("component_id", w.ComponentID)
	}
	val.Add("appid", w.config.ClientID)
	val.Add("redirect_uri", w.RerdirectUrl)
	val.Add("response_type", "code")
	scope := strings.Join(w.Scopes, ",")
	val.Add("scope", scope)
	val.Add("state", w.State)
	val.Add("connect_redirect", "1")
	return val
}

func (w *WechatConfig) getTokenFields(code string) url.Values {
	val := url.Values{}
	if w.isThird && w.ComponentID != "" {
		val.Add("appid", w.config.ClientID)
		val.Add("component_appid", w.ComponentID)
		val.Add("component_access_token", w.ComponentToken)
		val.Add("code", code)
		val.Add("grant_type", "authorization_code")
		return val
	}
	val.Add("appid", w.config.ClientID)
	val.Add("secret", w.config.ClientSecret)
	val.Add("code", code)
	val.Add("grant_type", "authorization_code")
	return val
}

func (w *WechatConfig) getPath() string {
	scope := gset.NewStrSetFrom(w.config.Scopes)
	if scope.Contains("snsapi_login") {
		return "qrconnect"
	}
	return "oauth2/authorize"

}

func (w *WechatConfig) getBaseUrl() string {
	return "https://api.weixin.qq.com/sns"
}

func (w *WechatConfig) getTokenUrl() string {
	if w.isThird && w.ComponentID != "" {
		return w.getBaseUrl() + "/oauth2/component/access_token"
	}
	return w.getBaseUrl() + "/oauth2/access_token"
}
