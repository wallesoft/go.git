module gitee.com/wallesoft/go

go 1.14

require (
	github.com/aliyun/alibaba-cloud-sdk-go v1.61.407
	github.com/gogf/gf/v2 v2.0.6
	github.com/json-iterator/go v1.1.10 // indirect
)
