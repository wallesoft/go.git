package sms

import (
	"sync"

	"github.com/gogf/gf/v2/container/gmap"
	"github.com/gogf/gf/v2/os/glog"
)

const (
	DEFAULT_GROUP_NAME = "default" // default config group name
)

// Config is the configuration management object.
type Config map[string]ConfigGroup

// ConfigGroup is a alice of configuration node for specifile named group.
type ConfigGroup []ConfigNode

//ConfigNode is configuration for one node.
type ConfigNode struct {
	// mu     sync.RWMutex
	Driver string          // sms driver
	Weight int             // weight for load balance
	Path   string          // template toml files storage path
	Config *gmap.StrAnyMap // dirver config
	Data   *gmap.StrStrMap // template data
}

// configs is internal used configuration object.
var configs struct {
	sync.RWMutex
	config Config // All configurations.
	group  string // Default configuration group
}

func init() {
	configs.config = make(Config)
	configs.group = DEFAULT_GROUP_NAME
}

// SetConfig sets the global configuration for sms.
// It will overwrite the old configuration of sms config
func SetConfig(config Config) {
	defer instances.Clear()
	configs.Lock()
	defer configs.Unlock()
	configs.config = config
}

// SetConfigGroup sets the configuration for given group.
func SetConfigGroup(group string, nodes ConfigGroup) {
	defer instances.Clear()
	configs.Lock()
	defer configs.Unlock()
	configs.config[group] = nodes
}

// AddConfigNode adds one node configuration to configuration of given group.
func AddConfigNode(group string, node ConfigNode) {
	defer instances.Clear()
	configs.Lock()
	defer configs.Unlock()
	configs.config[group] = append(configs.config[group], node)
}

// AddDefaultConfigNode adds one node configuration to configuration of default group.
func AddDefaultConfigNode(node ConfigNode) {
	AddConfigNode(DEFAULT_GROUP_NAME, node)
}

// AddDefaultConfigGroup adds multiple node configurations to configuration of default group.
func AddDefaultConfigGroup(nodes ConfigGroup) {
	SetConfigGroup(DEFAULT_GROUP_NAME, nodes)
}

// GetConfig retrieves and returns the configuration of given group.
func GetConfig(group string) ConfigGroup {
	configs.RLock()
	defer configs.RUnlock()
	return configs.config[group]
}

// SetDefaultGroup sets the group name for default configuration.
func SetDefaultGroup(name string) {
	defer instances.Clear()
	configs.Lock()
	defer configs.Unlock()
	configs.group = name
}

// GetDefaultGroup returns the { name of default configuration.
func GetDefaultGroup() string {
	defer instances.Clear()
	configs.RLock()
	defer configs.RUnlock()
	return configs.group
}

// SetLogger sets the logger for orm.
func (c *Core) SetLogger(logger *glog.Logger) {
	c.logger = logger
}

// GetLogger returns the logger of the orm.
func (c *Core) GetLogger() *glog.Logger {
	return c.logger
}

// SetDebug enables/disables the debug mode.
func (c *Core) SetDebug(debug bool) {
	c.debug.Set(debug)
}

// GetDebug returns the debug value.
func (c *Core) GetDebug() bool {
	return c.debug.Val()
}
