package sms

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"

	"github.com/aliyun/alibaba-cloud-sdk-go/services/dysmsapi"
	"github.com/gogf/gf/v2/container/gmap"
	"github.com/gogf/gf/v2/util/gconv"
)

// DriverAliyun is the driver for sms
type DriverAliyun struct {
	*Core
	Config *ConfigAliyun
	node   *ConfigNode
}

//ConfigAliyun aliyun sms config
type ConfigAliyun struct {
	AccessKeyId  string // accesskey id
	AccessSecret string // access secret
	Sign         string //signname
}

// New create new driver object and return
func (d *DriverAliyun) New(core *Core, node *ConfigNode) (SMS, error) {
	if node.Config == nil {
		return nil, errors.New("aliyun sms node config is nil")
	}
	var config *ConfigAliyun
	if err := gconv.Struct(node.Config.Map(), &config); err != nil {
		return nil, err
	}
	driver := &DriverAliyun{
		Core:   core,
		node:   node,
		Config: config,
	}
	if err := core.LoadTemplateFileToData(node); err != nil {
		return nil, err
	}
	return driver, nil
}
func (d *DriverAliyun) parseTemplateCode(name string) (string, error) {
	if d.node.Data.Contains(name) {
		return d.node.Data.Get(name), nil
	} else {
		d.logger.Error(context.TODO(), fmt.Sprintf("aliyun sms template '%s' is not exists", name))
	}
	return "", errors.New(fmt.Sprintf("aliyun sms template '%s' is not exists", name))

}

// Send send content to giving mobile
func (d *DriverAliyun) Send(ctx context.Context, mobile string, template string, params map[string]string) error {
	if code, err := d.parseTemplateCode(template); err == nil {
		client, err := dysmsapi.NewClientWithAccessKey("cn-hangzhou", d.Config.AccessKeyId, d.Config.AccessSecret)
		request := dysmsapi.CreateSendSmsRequest()
		request.Scheme = "https"
		request.PhoneNumbers = mobile
		request.SignName = d.Config.Sign
		request.TemplateCode = code
		m := gmap.NewStrStrMapFrom(params)
		paramStr, _ := json.Marshal(m)
		request.TemplateParam = string(paramStr)
		response, err := client.SendSms(request)
		if err != nil {
			d.logger.Error(context.TODO(), err.Error())
		}
		if response.Code == "OK" {
			return nil
		}
		d.logger.Error(context.TODO(), fmt.Sprintf("aliyun sms send error: Code: [%s],Msg:%s", response.Code, response.Message))
		return nil
	} else {
		return err
	}
}
