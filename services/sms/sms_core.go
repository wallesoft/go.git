package sms

import (
	"context"

	"github.com/gogf/gf/v2/container/gmap"
	"github.com/gogf/gf/v2/encoding/gjson"
	"github.com/gogf/gf/v2/os/gfile"
	"github.com/gogf/gf/v2/os/glog"
	"github.com/gogf/gf/v2/os/gres"
	"github.com/gogf/gf/v2/util/gconv"
)

// Send
func (c *Core) Send(ctx context.Context, mobile string, template string, params map[string]string) error {
	if err := c.SMS.Send(ctx, mobile, template, params); err != nil {
		return err
	}
	return nil
}

// LoadTemplateFileToData
// is only load once.
func (c *Core) LoadTemplateFileToData(node *ConfigNode) error {
	if node.Data != nil {
		return nil
	}
	path := node.Path + node.Driver + ".toml"
	if gres.Contains(path) {
		file := gres.Get(path)
		if node.Data == nil {
			node.Data = gmap.NewStrStrMap() //make(map[string]string)
		}
		// get content
		if i, err := gjson.LoadContent(file.Content()); err == nil {
			for k, v := range i.Map() {
				node.Data.Set(k, gconv.String(v))
			}
		} else {
			glog.Errorf(context.TODO(), "load sms template file '%s' failed: %v", node.Driver, err)
		}
	} else if path != "" {
		if !gfile.Exists(path) {
			glog.Errorf(context.TODO(), "load sms template file '%s' failed.", path)
		}
		// node.data = make(map[string]string)
		if node.Data == nil {
			node.Data = gmap.NewStrStrMap() //make(map[string]string)
		}
		if j, err := gjson.LoadContent(gfile.GetBytes(path)); err == nil {
			for k, v := range j.Map() {
				node.Data.Set(k, gconv.String(v))
			}
		} else {
			glog.Errorf(context.TODO(), "load sms template file '%s' failed: %v", node.Driver, err)
		}
	}

	// Hot reload >>> How to do ???
	return nil
}

// init initialize the driver template
func (c *Core) init() {

}
